PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./simple-weather.rb $(BINDIR)/simple-weather
	cp ./simple-weather.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/simple-weather
	rm $(BINDIR)/sw
	rm $(MANDIR)/simple-weather.1

