#!/usr/bin/env ruby
# coding: utf-8
#
# simple-weather - get plaintext weather information
#
# author: klosure
# https://gitlab.com/klosure/simple-weather
# license: BSD 3-clause


require "optparse"
require "json"
require "net/http"


# this class handles talking with OWM
class SW
  
  API_VER = "2.5"
  DEG_I = "°F"
  DEG_M = "°C"
  SPEED_I = "mph"
  SPEED_M = "kph"
  CONDS_EMOJI = { :Sunny => "☀", :Clear => "☀", :Rain => "☔", :Drizzle => "", :Mist => "☔",
                  :Clouds => "☁", :Fog => "☁", :Snow => "❄️", :Sleet => "❄️" }
  @@base_api_url = "https://api.openweathermap.org/data/#{API_VER}/weather"
  
  def initialize(k, loc, u="imperial")
    @api_key = k
    @location = loc
    if(u == "m")
      @units = "metric"
      @deg = DEG_M
      @speed = SPEED_M
    else
      @units = u
      @deg = DEG_I
      @speed = SPEED_I
    end
  end

  private def parse_resp(response, flags)
    ret_hash = JSON.parse(response)
    if(flags.key?(:condition))
      puts "#{ret_hash["weather"][0]["main"]} #{CONDS_EMOJI[ret_hash["weather"][0]["main"].to_sym]}"
    end
    if(flags.key?(:temp))
      puts "#{ret_hash["main"]["temp"]}#{@deg}"
    end
    if(flags.key?(:wind))
      puts "#{ret_hash["wind"]["speed"]} #{@speed}"
    end
    return true
  end
  
  private def get_request()
    uri = URI(@@base_api_url)
    params = { :q => @location, :units => @units, :appid => @api_key }
    uri.query = URI.encode_www_form(params)
    return Net::HTTP.get_response(uri)
  end

  # startup request/parse response action
  def run(flags)
    response = get_request()
    if response.is_a?(Net::HTTPSuccess)
      return parse_resp(response.body, flags)
    else
      puts "Error talking to OpenWeatherMap"
      puts response
      return false
    end
  end

end #end SW class


# parses through the args that were passed in
class ParseArgs

  VERSION = "0.1"

  def initialize()
    @args = {}
    @flags = {}
  end

  def parse()
    OptionParser.new do |parser|
      parser.banner = "Usage: simple-weather [options]"
      parser.on('-h', '--help', 'Show this help message') do |help|
        puts parser.to_s
        exit(0)
      end
      parser.on('-v', '--version', 'Show the version number') do |ver|
        @flags[:version] = true
        puts "simple-weather version: #{VERSION}"
        exit(0)
      end
      parser.on('-k', '--key=KEY', 'Your Open Weather Map API key (aka appid)') do |key|
        @args[:key] = key
      end
      parser.on('-l', '--key=LOCATION', 'Choose the location you wish to check') do |location|
        @args[:location] = location
      end
      parser.on('-u', '--units=[UNITS]', 'Choose imperial or metric units (default: imperial)') do |units|
        @args[:units] = units
      end
      parser.on('-t', '--temp', 'Return the current temperature') do |temp|
        @flags[:temp] = true
      end
      parser.on('-w', '--wind', 'Return the windspeed') do |wind|
        @flags[:wind] = true
      end
      parser.on('-c', '--condition', 'Return the "condition" i.e. clear, sunny, overcast, etc.') do |cond|
        @flags[:condition] = true
      end
    end.parse!
  end # end parse method

  
  private def check_args()
    if(!@args.key?(:key))
      puts "No API key passed in:"
        return false
    elsif(!@args.key?(:location))
      puts "No location specified:"
      return false
    else
      if(@args[:key].length != 32)
        puts "Bad API key length:"
        return false
      elsif(@args.key?(:units) && (@args[:units] != 'i' || @args[:units] != 'm'))
        puts "Bad units provided: use m or i"
        return false
      else
        return true
      end
    end
  end # end check_args


  def run()
    parse()
    if(!check_args())
      puts "Missing required argument(s)"
      puts "type 'simple-weather -h' to view the help info"
    else
      if(@args.key?(:units))
        sw_obj = SW.new(@args[:key], @args[:location], @args[:units])
      else
        sw_obj = SW.new(@args[:key], @args[:location])
      end
      return sw_obj.run(@flags)
    end
  end # end run

  
end # end class


# -- ENTRY POINT --
options = parse_obj = ParseArgs.new()
ok = parse_obj.run()
if(!ok)
  exit(1)
else
  exit(0)
end
