![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

A tool for receiving textual weather information.

### Dependencies

+ [Ruby](https://www.ruby-lang.org)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)

### Installation

```bash
# as root
make install

# to uninstall
make uninstall
```

The reasoning behind this tool is that it provides a way to get the bare minimum of information you need. All the CLI weather tools I've used, while nice, have not been very 'script-friendly'. This tool gives you only the bare minimum of what you ask for. Each item is separated by a new line. I've intentionally left out some items present in the OpenWeatherMap reponse simply because most of the time I never look at them.

> Note: You will need to sign up for a free OpenWeatherMap [API key](https://openweathermap.org) in order to use this tool

### Usage
```bash
simple-weather -t -l seattle -k myapikeyhere           # get the temperature in seattle"
simple-weather -c -l seattle -k myapikeyhere           # get the 'condition' i.e. sunny, cloudy, etc."
simple-weather -w -l seattle -k myapikeyhere           # get the windspeed"
simple-weather -tcw -u m -l seattle -k myapikeyhere    # get all three, and use metric units"
simple-weather -h                                      # show this help message"
simple-weather -v                                      # show version"

# note the order of arguments does not matter
```

### Limitations
- No zip code support
- English only
- Metric and Imperial units only (no Kelvin)

### License / Disclaimer
This project is licensed under the 3-clause BSD license. (See LICENSE.md)

I take no responsibility for you blowing stuff up.

Artwork courtesy of AcidBeast (CC 3.0 BY)
